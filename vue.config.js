const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  devServer:{
    proxy:'http://localhost:8000'
  },
  transpileDependencies: true,
  css:{
    loaderOptions:{
      sass:{
        // 加载css之前先执行什么代码，预加载
        additionalData:`
          @import "@/assets/scss/variable.scss";
          @import "@/assets/scss/mixin.scss";
        `
      }
    }
  }
})
