import { createRouter, createWebHistory } from 'vue-router'
import studentManage from './modules/student' // 学员
import course from './modules/course'    // 课程
import article from './modules/article'  // 文章
import home from './modules/home'
const routes = [
  {
    // 根路由主页
    path:'/',
    component:() => import(/* webpackChunkName: "layout" */ '@/Layout/layout.vue'),
    redirect:'/profile',
    meta:{
      title:'个人中心'
    },
    children:[
      {
        path:'/profile',
        name:'profile',
        component:()=>import(/* webpackChunkName: "profile" */ '@/views/profile/index.vue'),
        meta:{
          title:'个人中心'
        },
      },
    
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "login" */ '../views/Login/Login.vue')
  },
  {
    path: '/401',
    name: '401',
    component: () => import(/* webpackChunkName: "401" */ '@/views/error-page/401.vue')
  },
  {
    path: '/404',
    name: '404',
    component: () => import(/* webpackChunkName: "404" */ '@/views/error-page/404.vue')
  },
  studentManage,
  course,
  article,
  home
]

export const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
