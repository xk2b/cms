export default 
{
    path:'/student',
    component:()=> import('@/Layout/layout.vue'),
    redirect:'/student/studentManage',
    name:'student',
    meta:{
        title:'学员',
        icon:'personnel'
    },
    children:[
        {
            path:'studentManage',
            component:()=>import('@/views/student-manage/index.vue'),
            meta:{
                title:'学员管理',
                icon:'personnel-manage'
            },
        }
    ]
}