export default 
{
    path:'/article',
    name:'article',
    component:()=> import(/* webpackChunkName: "layout" */ '@/Layout/layout.vue'),
    redirect:'/article/ranking',
    meta:{
        title:'文章',
        icon:'article',
    },
    children:[
        {
            path:'ranking',
            name:'ranking',
            component:()=>import(/* webpackChunkName: "article-ranking" */ '@/views/article-ranking'),
            meta:{
                title:'文章排名'
            }
        },
        {
            path:'create',
            name:'create',
            component:()=>import(/* webpackChunkName: "article-create" */ '@/views/article-create'),
            meta:{
                title:'创建文章'
            }
        }
    ]
    
}