export default 
{
    path:'/course',
    name:'course',
    component:()=> import('@/Layout/layout.vue'),
    redirect:'/course/courseManage',
    meta:{
        title:'课程',
    },
    children:[
        {
            path:'courseManage',
            component:()=> import(/* webpackChunkName: "course-manage" */ '@/views/course-manage'),
            meta:{
                title:'课程管理',
            }
        },
        {
            path:'classManage',
            component:()=> import('@/views/course-class'),
            meta:{
                title:'班期管理',
            }
        },
        {
            path:'live',
            component:()=> import('@/views/course-live'),
            meta:{
                title:'我的直播',
            }
        },
    ]
    
}