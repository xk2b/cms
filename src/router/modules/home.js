export default {
    path: '/home',
    component: () => import('@/Layout/layout.vue'),
    meta: {
        title: '首页',
    },
    children: [
        {
            path: '/home',
            component: () => import('@/views/home/home.vue'),
            meta: {
                title: '首页'
            },
        },
    ]
}
