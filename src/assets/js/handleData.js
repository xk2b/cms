import {userRouteStore} from '@/store/route'
export default ()=>{
    const routeStore = userRouteStore()
    const menuRoute = routeStore.menuRoute
    let newArr = menuRoute.map(item=>{
        item.titles = [item.title]
        return item
    })
    menuRoute.forEach(item=>{
        if (item.children.length){
            // 代表有二级路由
            item.children.forEach(child=>{
                child.titles = item.titles.concat(child.title)
            })
            newArr.push(...item.children)
        }
        
    })
    // console.log(newArr);
    return newArr
}