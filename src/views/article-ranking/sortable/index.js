import Sortable from "sortablejs";
import { ref } from "vue";
import { reviseRank } from '@/apis/article'
import { ElMessage } from "element-plus";

export const tableRef = ref(null)

export const initSorTable = (articleList,cb)=>{
    // 获取拖拽表格tbody元素
    let el = tableRef.value.$el.querySelector('.el-scrollbar__view tbody')

    // 初始化sortable
    Sortable.create(el,{
        // 配置拖拽时的类名
        ghostClass:'sortable-ghost',

        // 拖拽结束后触发的钩子函数
       async onEnd(e){
        // e:事件对象 替换的元素和被替换元素都信息都在这里面
        const {newIndex,oldIndex} = e // 被切换的索引值
        // console.log('新的:',newIndex,'旧的:',oldIndex);

        const {data} = await reviseRank({man:articleList.value[newIndex],woman:articleList.value[oldIndex]})
        if (!data.code){
            ElMessage.success(data.txt)
            // 强制 el-table感应
            articleList.value = []
            cb()
        }
        }
    })
}