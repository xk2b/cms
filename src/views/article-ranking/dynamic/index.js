import {ref,computed} from 'vue'

const dynamicRaw = [
    {label:'排名',prop:'rank'},
    {label:'标题',prop:'title'},
    {label:'作者',prop:'author'},
    {label:'日期',prop:'date'},
    {label:'描述',prop:'description'},
]

export const dynamiData = ref(dynamicRaw) // 源数据,固定的数据我们不会修改它


// 选框的选值
export const selectDynamic = ref([])


// 初始化复选框的默认选值,默认是全部选中的
const initSelectDynamic = ()=>{
    selectDynamic.value = dynamiData.value.map(item=>item.label)
}
initSelectDynamic()


// 表格中的项
export const tableColumns = computed(()=>{
    return dynamiData.value.filter(item=>selectDynamic.value.includes(item.label))
})