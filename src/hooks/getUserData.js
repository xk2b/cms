// 获取用户的头像
import { userStore } from '@/store/user';
import { computed } from 'vue';
export const Getdata = ()=>{
    const user= userStore()
    const userdata = computed(()=>{
        if (user.userInfo.photo){
            return user.userInfo
            
        }else{
             user.userInfo.photo = require('@/assets/default.png')
        }
        
        return user.userInfo
    })
    return {
        userdata
    }
}