import { ElMessageBox } from "element-plus"
export const quiFn = ({contenTxt,titleTxt,confirmButtonText,cancelButtonText,type},confirmCb,cancelCb)=>{
    ElMessageBox.confirm(contenTxt || '当前的内容还未保存,确定取消吗?',titleTxt || '取消保存',{
        confirmButtonText:confirmButtonText || '确认',
        cancelButtonText:cancelButtonText || '取消',
        type:type|| 'warning'
    }).then(()=>{
        // 用户点击了确认按钮   || 过假留真， &&过真留假
        confirmCb && confirmCb() // 没传回调也不会报错
    }).catch(()=>{
        // 用户点击了取消按钮
        cancelCb && cancelCb()
    })
}