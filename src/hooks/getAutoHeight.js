import { onActivated, onDeactivated, onMounted, onUnmounted, ref } from "vue"

export const getAutoHeight = (marginTop)=>{
    const tableHeight = ref(window.innerHeight-marginTop)
    const refresHeight = ()=>{
        tableHeight.value = window.innerHeight-marginTop
    }
    onMounted(()=>{
        window.addEventListener('resize',refresHeight)
    })
    onUnmounted(()=>{
        window.removeEventListener('resize',refresHeight)
    })
    onActivated(()=>{
        window.addEventListener('resize',refresHeight)
    })
    onDeactivated(()=>{
        window.removeEventListener('resize',refresHeight)
    })
    return tableHeight
}