import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import '@/assets/scss/index.scss'
import ElementPlus from 'element-plus' // ui库
import 'element-plus/dist/index.css' // ui样式库
import * as ElementPlusIconsVue from '@element-plus/icons-vue' // 图标组件
import {createPinia} from 'pinia'
import permission from '@/utils/permission' // 校验登录，导入它就会自动执行
import zhCn from 'element-plus/es/locale/lang/zh-cn'
// import '@/utils/flexible' // 淘宝无限适配
import '@toast-ui/editor/dist/toastui-editor.css'
const app = createApp(App)

// 循环注册图标组件
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}




// pinia实例 
const pinia = createPinia()
export default pinia

app.use(router)
.use(ElementPlus,{locale:zhCn}) // ui组件配置中文
.use(pinia)
.mount('#app')

 