import {defineStore} from 'pinia'
import {getRoute} from '@/apis/menuRoute' // 向后端请求的api
import {getLocal,setLocal} from '../utils/storage'
export const userRouteStore = defineStore('ROUTE',{
    state:()=>{
        return{
            menuRouteRaw:getLocal('__menuRoute__',[])
        }
    },
    actions:{
      getMenuRouteFn(){
        getRoute().then(({data})=>{
          this.menuRouteRaw = data
          setLocal('__menuRoute__',data)
        })
      },
    },
    getters:{
      // 当用户未登录时，就只给返回个人中心这个路由
      menuRoute(){
        return this.menuRouteRaw.length ?  this.menuRouteRaw: [{
          children:[],
          icon:'User',
          path:'/profile',
          title:'个人中心'
        }]
      }
    },
})