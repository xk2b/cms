import {defineStore} from 'pinia'
import {getLocal,setLocal} from '../utils/storage'
import getRoutePool  from '@/assets/js/handleData'
export const userappStore = defineStore('APP',{
    state:()=>{
        return{
            tagsViewList:getLocal('__tagsViewList__',[])
        }
    },
    actions:{
        // 新增tag
        addTagsViewList(tag){
            // 判断新增tag标签是否存在于我们定义的路由中401 404除外
            const routePool = getRoutePool()
            const isFind = routePool.find(item=>item.path===tag.path)
            // 判断是否是菜单路由
            if (!isFind)return // 取反就是当存在时就继续往下走，反之就直接返回终止

            // 判断是否在 tagsViewList中已经存在
            const isExit =  this.tagsViewList.find(item=>item.path===tag.path)

            // 不存在添加至tagsViewList中
            if (!isExit){
                // 存储到大菠萝
                this.tagsViewList.push(tag)
                // 存储到本地进行持久化
                setLocal('__tagsViewList__',this.tagsViewList)
            }            
        },

        // 删除tag
        removeTagsViewList(type,index){
            // 0 删本身  1删除了本身所有的 2删右侧
            let tagsViewList = this.tagsViewList
            
            switch (type){
                case 0:
                    tagsViewList.splice(index,1)
                    break
                case 1:
                    tagsViewList = tagsViewList.splice(index,1)
                    break
                case 2:
                    tagsViewList.splice(index+1)
                    break
            }
            this.tagsViewList = tagsViewList
            setLocal('__tagsViewList__',this.tagsViewList)
        }
    },
})