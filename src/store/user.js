import {defineStore} from 'pinia'
import { ElMessage } from 'element-plus'
import {login,logout,checkLogin} from '@/apis/login' // 登录接口// 退出登录接口// 校验登录接口
import {userRouteStore} from './route'  // 路由
import { router } from '@/router/index'
import {getLocal,setLocal,removeAllItem} from '../utils/storage' // 本地存储
import pinia from '@/main' // 大菠萝的实例对象



export const userStore = defineStore('USER',{
    state:()=>{
        return{
            userInfo:getLocal('__userInfo__',{})
        }
    },
    actions:{
        // 发送登录请求
        loginFn(value){ 
            login(value).then(({data})=>{
                // 登录成功，将用户信息保存至本地缓存
                switch (data.code){
                    case 0:
                        // 先保存一份到大菠萝
                        this.userInfo = data.data
                        // 缓存至本地
                        setLocal('__userInfo__',data.data)
                        ElMessage.success({
                            message:'登录成功',
                            customClass:'prompt'
                        })
                        // 预留位置进行路由跳转,
                        // 获取侧边菜单路由
                        const routeStore = userRouteStore()
                        routeStore.getMenuRouteFn() // 获取路由菜单数据
                        router.push('/')
                        break
                    case 1:
                        ElMessage.error({
                            message:data.txt,
                            customClass:'prompt'
                        })
                        break
                    case 2:
                        ElMessage.error({
                            message:data.txt,
                            customClass:'prompt'
                        })
                }
            })
        // 退出登录

        },
        logoutFn(){
            logout().then(({data})=>{
                if (data.code == '0'){
                    // 退出成功清空用户信息
                    removeAllItem()
                    // 清空大菠萝数据
                    const allStore = [...pinia._s.values()]
                    allStore.forEach(item=>{
                        item.$reset()
                    })
                    ElMessage.success({
                        message:'退出成功',
                        customClass:'prompt',
                        grouping:true,
                    })
                    router.push('/login')
                }
            })
        },
        checkLoginFn(){
            return new Promise((res,rej)=>{
                checkLogin().then(({data})=>{
                    if (!data.code){
                        // 已登录，将最新数据保存
                        this.userInfo = data.data
                        setLocal('__userInfo__',data.data)
                    }else{
                        this.userInfo =  {}
                        setLocal('__userInfo__',{})
                        // 清空登录过的用户才能访问的路由地址
                        setLocal('__menuRoute__',{})
                    }
                    // 传递成功的状态  0表示登录，1表示未登录
                    res(data.code)
                })
            })

        }
    },
})