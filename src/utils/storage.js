// 获取会话存储值
export const getSession = (key,type)=>{
    let item = JSON.parse(sessionStorage.getItem(key))
    // 如果没有该值，就将你预设的type返回
    if (!type){
        item = type
    }
    return item
}

// 设置会话存储值
export const setSession = (key,item)=>{
    return sessionStorage.setItem(key,JSON.stringify(item))
}

// 获取永久存储值
export const getLocal = (key,type)=>{
    let item = JSON.parse(localStorage.getItem(key))
    if (String(item)==='null'){
        item=type
    }
    return item
}

// 设置永久存储值
export const setLocal = (key,item)=>{
    return localStorage.setItem(key,JSON.stringify(item))
}

// 清空会话存储和本地存储
export const removeAllItem = ()=>{
    localStorage.clear()
    sessionStorage.clear()
}