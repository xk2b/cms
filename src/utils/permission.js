import router from "@/router";
import {userStore} from '@/store/user'

// 定义白名单,未登录也能访问的路由
const whiteList = [
    '/login',
    '/401',
    '/404',
]

// 全局守卫，当要进行路由跳转时，先在这里校验一遍
router.beforeEach(async (to,from,next)=>{
    // 用户登录时，判断是否在路由表中
    const allRoute = router.getRoutes() // 获取所有路由
    const isExist = allRoute.some((item)=>item.path==to.fullPath) // 判断要跳转的路由是否在路由表中

    if (isExist){
        const store = new userStore()
        // 校验是否登录
        const isLoginCode  = await store.checkLoginFn()  // 0 代表登录 1 未登录
        // 当未登录时，判断路由是否在白名单中
        if (isLoginCode){ 
            if (whiteList.includes(to.fullPath)){
                return next()
            }else{ // 没有在白名单中跳转至401，表示没权限
                return next('/401')
            }
        }
        // 处于登录状态直接跳转
        return next()
    }
    // 当需要跳转的路由不存在与路由表中时跳转至404页面
    return next('/404')
})