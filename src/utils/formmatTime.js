// 时间转换方法
export const formatTime = (time)=>{
    // 2023-09-16 15:47:42 最终的时间格式
    let date = new Date(time / 1);
    let year = date.getFullYear(); // 年
    let month = date.getMonth()+1 < 10 ?  '0' + (date.getMonth()+1) : date.getMonth()+1; // 月
    let day = date.getDate() < 10 ?  '0' + date.getDate() :  date.getDate(); // 日
    let hour = date.getHours() < 10 ?  '0' + date.getHours() :  date.getHours(); // 时
    let minute = date.getMinutes() < 10 ?  '0' + date.getMinutes() :  date.getMinutes();  // 分
    let second = date.getSeconds() < 10 ?  '0' + date.getSeconds() :  date.getSeconds();
    return `${year}-${month}-${day} ${hour}:${minute}:${second}`
}

export const formaDate  = (time,n)=>{
    // n不为空返回年月日星期   反之只返回时间
    let date = new Date(time / 1); // 除1是强制类型转换
    let year = date.getFullYear(); // 年
    let month = date.getMonth()+1 < 10 ?  '0' + (date.getMonth()+1) : date.getMonth()+1; // 月
    let day = date.getDate() < 10 ?  '0' + date.getDate() :  date.getDate(); // 日
    let hour = date.getHours() < 10 ?  '0' + date.getHours() :  date.getHours(); // 时
    let minute = date.getMinutes() < 10 ?  '0' + date.getMinutes() :  date.getMinutes();  // 分
    let second = date.getSeconds() < 10 ?  '0' + date.getSeconds() :  date.getSeconds();
    let week = date.getDay();

    if (n){
        switch (week){
            case 0:
                week = '星期日'
                break;
            case 1:
                week = '星期一'
                break;
            case 2:
                week = '星期二'
                break;
            case 3:
                week = '星期三'
                break;
            case 4:
                week = '星期四'
                break;
            case 5:
                week = '星期五'
                break;
            case 6:
                week = '星期六'
                break;
        }
        return `${year}-${month}-${day} ${week}`
    }else{
        return `${hour}:${minute}:${second}`
    }
}
