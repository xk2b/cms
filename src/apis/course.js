import request from '@/utils/request'


// 获取课程信息
export const getCourse = (params)=>{
    return request({
        url : '/course/courseList',
        params:params
    })
}

 

// 修改课程封面
export const reviseCover= (data)=>{
    return request({
        method:'post',
        url : '/course/reviseCover',
        data:data
    })
}

// 修改课程基础信息
export const reviseCourseBase = (data)=>{
    return request({
        method:'post',
        url : '/course/reviseCourseBase',
        data:data
    })
}