import request from '@/utils/request'


// 新增文章接口
export const addArticle = (data)=>{
    return request({
        method:'post',
        url : '/article/addArticle',
        data:data
    })
} 

// 获取文章接口
export const getArticleList = (params)=>{
    return request({
        url : '/article/articleList',
        params:params
    })
}

// 修改文章
export const reviseRank = (data)=>{
    return request({
        method:'post',
        url : '/article/reviseRank',
        data:data
    })
}


// 删除文章
export const deleteArticle = (data)=>{
    return request({
        method:'post',
        url : '/article/deleteArticle',
        data:data
    })
}