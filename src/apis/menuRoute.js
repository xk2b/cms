import request from '@/utils/request' // 导入axios实例

// 请求后台路由菜单
export const getRoute = ()=>{
    return request({
        url:'/data/route',
    })
}