import request from '@/utils/request' // 导入axios实例

export const login = (formData)=>{
    return request({
        // 只需要写路由即可，已经配置vue代理了
        url:'/login',
        method:'POST',
        data:formData
    })
}

// 退出登录
export const logout = ()=>{
    return request({
        url:'/login/logout',
    })
}

// 校验登录
export const checkLogin = ()=>{
    return request({
        url:'/login/checkLogin',
    })
}