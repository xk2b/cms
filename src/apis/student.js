import request from '@/utils/request' // 导入axios实例

//  获取学员列表
export const getStudentList = (params)=>{
    return request({
        url:'/student/studentList',
        params:params
    })
}

// 获取课程分类对象
export const getCourseObj = ()=>{
    return request({
        url:'/student/courseObj',
    })
}

// 修改学员信息
export const reviseStudent = (data)=>{
    return request({
        url:'/student/reviseStudent',
        method:'post',
        data:data
    })
}


// 修改班期
export const reviseStudentClass = (data)=>{
    return request({
        url:'/student/reviseStudentClass',
        method:'post',
        data:data
    })
}


// 修改课程是否启用
export const reviseIsEnable = (data)=>{
    return request({
        url:'/student/reviseIsEnable',
        method:'post',
        data:data
    })
}


// 修改课程是否启用
export const addStudent = (data) => {
    return request({
        url: '/student/addStudent',
        method: 'post',
        data: data
    })
}