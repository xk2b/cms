import request from '@/utils/request'

// 新增直播
export const addLive = (data)=>{
    return request({
        method:'post',
        url : '/live/addlive',
        data:data
    })
}

// 删除直播
export const deleteLive = (data)=>{
    return request({
        method:'post',
        url : '/live/deleteLive',
        data:data
    })
}

// 修改直播
export const reviseLive = (data)=>{
    return request({
        method:'post',
        url : '/live/reviseLive',
        data:data
    })
}

// 获取直播列表
export const getLiveList = (params)=>{
    return request({
        url : '/live/liveList',
        params
    })
}