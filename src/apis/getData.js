import request from '@/utils/request' // 导入axios实例


// 获取网站的功能信息
export const getFeature = ()=>{
    return request({
        // 只需要写路由即可，已经配置vue代理了
        url:'/data/feature',
    })
}